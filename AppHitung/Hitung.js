import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from "react-native";

const Hitung = () => {
    const [panjang, setPanjang] = useState("");
    const [lebar, setLebar] = useState("");
    const [luas, setLuas] = useState("");
    const [volume, setVolume] = useState("");
    const [tinggi, setTinggi] = useState("");

    const hitungLuas=(panjang, lebar)=>{
      const temp = panjang*lebar;
      setLuas(temp);
    }

    const hitungVolume=(panjang, lebar, tinggi)=>{
      const temp = panjang*lebar*tinggi;
      setVolume(temp);
    }

    return (
      <View style={styles.container}>
        <View style={styles.posTitle}>
          <Text style={styles.title}>Menghitung Luas Permukaan dan Volume Persegi Panjang</Text>
        </View>

          <View style={styles.contInput}>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan panjang"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setPanjang(value)}
                  value={panjang}
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan lebar"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setLebar(value)}
                  value={lebar}
                />
            </View>
            <View style={styles.posInput}>
                <TextInput 
                  style={styles.input}
                  placeholder="Masukkan tinggi"
                  placeholderTextColor="#0066ff"
                  onChangeText={(value)=>setTinggi(value)}
                  value={tinggi}
                />
            </View>
            <View style={styles.posButton}>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungLuas(panjang, lebar)}}
                >
                  <Text style={styles.textButton}>Hitung Luas</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.button}
                  onPress={()=>{hitungVolume(panjang, lebar, tinggi)}}
                >
                  <Text style={styles.textButton}>Hitung Volume</Text>
                </TouchableOpacity>
            </View>
          </View>
          
        <View style={styles.posOutput}>
          <Text>Luas Persegi Panjang </Text>
          <Text style={styles.textOutput}>{luas}</Text>
        </View>
        <View style={styles.posOutput}>
          <Text>Volume Persegi Panjang </Text>
          <Text style={styles.textOutput}>{volume}</Text>
        </View>
      </View>
    )
};

const styles = StyleSheet.create({
    container:{
      marginTop: 40,
      width: 1300,
      height: 900,
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1
    },
    posTitle:{
      alignItems: 'center',
    },
    title:{
      fontSize : 18,
      fontWeight : 'bold'
    },
    contInput:{
      backgroundColor:'#0066ff',
      margin: 20,
      padding: 15,
      borderRadius: 15
    },
    posInput:{
      marginLeft : 20,
      marginRight : 20,
      marginBottom : 10,
      backgroundColor : '#99ccff',
      paddingLeft : 10,
      paddingRight: 10,
    },
    input:{
      height : 30
    },
    posButton:{
      margin: 20,
      alignItems:'center',
      flexDirection: 'row',
      justifyContent: 'center'
    },
    button:{
      borderRadius: 20,
      width: 180,
      height: 30,
      alignItems:'center',
      backgroundColor : '#ccffff',
      justifyContent : 'center',
      marginHorizontal: 5
    },
    textButton:{
      fontWeight: 'bold',
      color: '#0066ff'
    },
    posOutput:{
      alignItems:'center',
      marginVertical: 10
    },
    textOutput:{
      fontSize: 30,
      fontWeight: 'boldx',
      marginVertical: 5
    }
})

export default Hitung;
