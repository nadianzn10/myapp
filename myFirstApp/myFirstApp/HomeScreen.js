import React from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'
import logo from './assets/program.png'

export default class App extends React.Component {
  render() {

    this.props.navigation.setOptions({
        headerBackTitle: '',
        headerShown: false,
    })
    
    return (
      <View style={styles.container}>
        <Image
          source={logo}
          style={styles.logo}
        />

        <Text style={styles.text}>HAI!</Text>

        <View style={styles.tombol}>
          <TouchableOpacity
          onPress={ () => this.props.navigation.navigate('Login')}
            style={styles.input}
          >
            <Text style={styles.masuk}>LOGIN</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }
}const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(95, 158, 160)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    width: 120,
    height: 120,
    alignItems: 'center',
    resizeMode: 'contain'
  },

  text: {
    color: 'black',
    fontSize: 40,
    fontWeight: 'bold'
  },

  tombol: {
    flexDirection: 'row',
    margin: 20,
    paddingVertical: 20
  },

  input: {
    width: 150,
    height: 45,
    borderRadius: 30,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgb(169, 169, 169)',
    color: 'rgb(47, 79, 79)',
    marginHorizontal: 2,
    borderWidth: 1,
    borderColor: 'rgb(127, 255, 0)'
  },
})