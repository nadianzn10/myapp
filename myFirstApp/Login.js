import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native'


export default class Login extends React.Component {

    render(){
        return(
            <View style= {styles.container}>
                <Text style={{ fontSize: 22, marginTop: 20 }}>Welcome</Text>
                <Text style={{ fontSize: 16, color: 'gray', marginTop: 20 }}>Sign In to continue</Text>

                <TextInput
                    style = {{ marginTop: 40, borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom: 20 }}
                    placeholder = "Username"
                />

                <TextInput
                    style = {{ marginTop: 40, borderBottomColor: '#ddd', borderBottomWidth: 1, paddingBottom: 20 }}
                    placeholder = "Password"
                />

                <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 40 }}>
                    <TouchableOpacity  style={{ width: 100, backgroundColor: 'rgb(95, 158, 160)', 
                        paddingBottom: 10, alignItems: 'center', justifyContent: 'center',
                        borderRadius: 40, marginTop: 30 }}>
                       

                        <Text style={{ textAlign: 'center', color: '#FFF', fontSize: 16}}>Login Now</Text>

                    </TouchableOpacity>

                    <Text style={{ marginTop: 20 }}>Forgot Password? </Text>
                    
                    <View style= {{ flexDirection: 'row', marginTop: 60}}>
                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#3f51b5',
                         alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>f 
                        </Text>
                        </View>

                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#f44336',
                         marginHorizontal: 10, alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>G
                        </Text>
                        </View>

                        
                        <View style={{ height: 40, width: 40, borderRadius: 40/2, backgroundColor: '#1565c0',
                         alignItems: 'center', justifyContent: 'center'}}>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#FFF' }}>In 
                        </Text>
                        </View>
                    </View>

                    <View style= {{ flexDirection: 'row', marginTop: 40 }}>
                    <Text style={{ color: 'grey' }}>Don't have an account? </Text>
                    <Text style={{ fontWeight: 'bold' }}>Sign Up </Text>
                    </View>

            </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 20
    }
})